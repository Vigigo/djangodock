#!/bin/bash

# Copy base uwsgi configuration
cp /srv/uwsgi.base.ini /srv/uwsgi.ini

# Set Django project name
echo "module = "`cat /srv/django/manage.py | grep -Po '[^"\s]+.settings' | awk -F. '{ print $1 }'`".wsgi:application"  >> /srv/uwsgi.ini
echo "env DJANGO_SETTINGS_MODULE="`cat /srv/django/manage.py | grep -Po '[^"\s]+.settings' | awk -F. '{ print $1 }'`".settings" >> /srv/uwsgi.ini

# Install requirements.txt
su djangodock /bin/bash -c ". /srv/env/bin/activate && pip install -r /srv/django/requirements.txt"
